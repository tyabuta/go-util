package main

import (
	"fmt"
	"os"
	"os/user"
	"path"

	"gitlab.com/tyabuta/go-util"
)

func workDirRoot() string {
	usr, _ := user.Current()
	return usr.HomeDir + "/workdir"
}

func main() {
	dir := path.Join(workDirRoot(), util.DateKeyNow())
	if err := os.MkdirAll(dir, 0755); nil != err {
		fmt.Println(err)
		os.Exit(1)
	}
	if err := os.Chdir(dir); nil != err {
		fmt.Println(err)
		os.Exit(1)
	}
	fmt.Println(dir)
}
