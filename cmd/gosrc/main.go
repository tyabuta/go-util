package main

import (
	"fmt"
	"os"
	"path"

	"github.com/urfave/cli"
	"gitlab.com/tyabuta/go-util"
)

func doList(c *cli.Context) error {
	printFullPaths := c.Bool("full-path")

	if printFullPaths {
		fmt.Println(util.CmdExecOutput("ls -1d $GOPATH/src/*/*/*"))
	} else {
		fmt.Println(util.CmdExecOutput("ls -1d $GOPATH/src/*/*/* | perl -pe \"s|$GOPATH/src/||\""))
	}

	return nil
}

func doLook(c *cli.Context) error {
	name := c.Args().First()

	gopath := os.Getenv("GOPATH")
	if "" == gopath {
		fmt.Println("undefinded GOPATH")
		os.Exit(1)
	}

	srcPath := path.Join(gopath, "src", name)
	if !util.DirectoryExists(srcPath) {
		fmt.Println("Not found Path: " + srcPath)
		os.Exit(1)
	}

	fmt.Println("go to \"" + srcPath + "\"")

	util.ShellLaunch(srcPath)
	return nil
}

var commandList = cli.Command{
	Name:   "list",
	Usage:  "List $GOPATH/src directories",
	Action: doList,
	Flags: []cli.Flag{
		cli.BoolFlag{Name: "full-path, p", Usage: "Print full paths"},
	},
}

var commandLook = cli.Command{
	Name:   "look",
	Usage:  "Go to src directory",
	Action: doLook,
}

var app *cli.App

func init() {
	app = cli.NewApp()
	app.Name = "gosrc"
	app.Usage = "Access to go-src directories"
	app.Version = "0.1.0"
	app.Author = "tyabuta"
	app.Email = "gmforip@gmail.com"
	app.Commands = []cli.Command{
		commandList,
		commandLook,
	}
}

func main() {
	app.Run(os.Args)
}
