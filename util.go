package util

import (
	"os"
	"os/exec"
	"syscall"
	"time"
	"strings"
)

func DirectoryExists(path string) bool {
	fileInfo, err := os.Stat(path)
	return err == nil && fileInfo.IsDir()
}

func FileExists(path string) bool {
	fileInfo, err := os.Stat(path)
	return err == nil && !fileInfo.IsDir()
}

func FileExtension(path string) string {
	pos := strings.LastIndex(path, ".")
	if -1 == pos {
		return ""
	}
	ext := path[pos:]
	return strings.ToLower(ext)
}

func CmdWhich(cmd string) bool {
	_, err := exec.LookPath(cmd)
	return err == nil
}

func ShellLaunch(dir string) {
	shell := os.Getenv("SHELL")
	if "" == shell {
		shell = "/bin/sh"
	}

	os.Chdir(dir)
	env := syscall.Environ()
	syscall.Exec(shell, []string{shell}, env)
}

func CmdExecOutput(command string) string {
	out, err := exec.Command("sh", "-c", command).Output()
	if err != nil {
		panic(err)
	}
	return string(out)
}

func DateKeyNow() string {
	t := time.Now()
	return t.Format("2006-01-02")
}

func Space(count int) string {
	buf := make([]byte, 0, count)
	for i := 0; i < count; i++ {
		buf = append(buf, ' ')
	}
	return string(buf)
}


