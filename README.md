go-util
=======

## 全コマンドインストール

```bash
go get -v gitlab.com/tyabuta/go-util/cmd/...
```

## gosrc

コマンドインストール

```bash
go get -v gitlab.com/tyabuta/go-util/cmd/gosrc
```

zshへの設定方法

```bash
gosrc-look (){
    local selectedDir=$(gosrc list | fzf)
    gosrc look $selectedDir
}
```


