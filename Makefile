

test:
	go test -v .

build:
	ls ./cmd | xargs -I{} go build -o bin/{} ./cmd/{}

clean:
	rm -rf bin

install:
	go install ./cmd/wd
	go install ./cmd/gosrc



# for gosrc
setup:
	go get github.com/urfave/cli

