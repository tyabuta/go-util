package util

import (
	"testing"
)

func TestUtils(t *testing.T) {

	t.Log("DirectoryExists")
	if !DirectoryExists(".") {
		t.Error("not working")
	}
	if DirectoryExists("hoge") {
		t.Error("not working")
	}

	t.Log("FileExists")
	if !FileExists("./util.go") {
		t.Error("not working")
	}
	if FileExists("hoge") {
		t.Error("not working")
	}

	t.Log("FileExtension")
	if ".piyo" != FileExtension("hoge.piyo") {
		t.Error("not working")
	}
	if ".piyo" != FileExtension("hoge.PIYO") {
		t.Error("not working")
	}
	if "" != FileExtension("hogepiyo") {
		t.Error("not working")
	}

	t.Log("CmdWhich")
	if !CmdWhich("ls") {
		t.Error("not working")
	}
	if CmdWhich("hoge") {
		t.Error("not working")
	}

	t.Log("CmdExecOutput")
	if "" == CmdExecOutput("ls") {
		t.Error("not working")
	}

	t.Log("DateKeyNow")
	t.Log("==>", DateKeyNow())

	t.Log("Space")
	if "   " == Space(4) {
		t.Error("not working")
	}

}
